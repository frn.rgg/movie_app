import React from "react";

import YRMlogo from "../../image/yourmovies.png";

import { Wrapper, Content, LogoImg } from "./Header.styles";


const Header = () => (

    <Wrapper>
        <Content>

            <LogoImg src={YRMlogo} alt="image-logo" />
            
        </Content>
    </Wrapper>

)

export default Header;